package co.makery

import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get

@Controller
class GreetingController {

    @Get("/greet/{name}")
    fun greet(name: String): String = "Hello $name!"

    // http localhost:8080/greet/Makers
}
