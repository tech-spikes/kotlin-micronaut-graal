import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    application
    id("com.github.johnrengelman.shadow") version Version.Build.shadow
    id("io.spring.dependency-management") version Version.Build.dependencyManagement
    id("org.jetbrains.kotlin.jvm") version Version.kotlin
    id("org.jetbrains.kotlin.kapt") version Version.kotlin
    id("org.jetbrains.kotlin.plugin.allopen") version Version.kotlin
}

version = "0.1.0"
group = "co.makery"

repositories {
    mavenLocal()
    mavenCentral()
    jcenter()
}

dependencyManagement {
    imports {
        mavenBom("io.micronaut:micronaut-bom:${Version.micronaut}")
    }
}

dependencies {
    compile("com.fasterxml.jackson.module:jackson-module-kotlin:${Version.jackson}")
    compile("io.micronaut:micronaut-inject-java")
    compile("io.micronaut:micronaut-runtime")
    compile("io.micronaut:micronaut-http-server-netty")
    compile("org.jetbrains.kotlin:kotlin-reflect")
    compile("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    compileOnly("com.oracle.substratevm:svm")
    kapt("io.micronaut:micronaut-inject-java")
    runtime("ch.qos.logback:logback-classic:${Version.logback}")
    runtime("io.micronaut:micronaut-graal")
}

val run by tasks.getting(JavaExec::class) { jvmArgs("-noverify", "-XX:TieredStopAtLevel=1", "-Xmx25M") }

val shadowJar by tasks.getting(ShadowJar::class) { mergeServiceFiles() }

val compileKotlin by tasks.getting(KotlinCompile::class) {
    kotlinOptions {
        jvmTarget = "1.8"
        javaParameters = true
    }
}

val compileTestKotlin by tasks.getting(KotlinCompile::class) {
    kotlinOptions {
        jvmTarget = "1.8"
        javaParameters = true
    }
}

allOpen { annotation("io.micronaut.aop.Around") }

application { mainClassName = "co.makery.Application" }
