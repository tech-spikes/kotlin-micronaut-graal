# Example Micronaut & Graal integration

This project contains a Hello World Micronaut application configured with GraalVM integration. It is based on the [official Graal integration guide](https://docs.micronaut.io/latest/guide/index.html#graal).

## Setup

To be able to compile the native image, you have to use GraalVM as your JVM and export the `native-image` to your `PATH`.

You can download GraalVM from [here](https://github.com/oracle/graal/releases).

Setting up the environment:

```
export GRAALVM_DIR=<PATH-TO>/graalvm-ce-1.0.0-rc12
export PATH=$GRAALVM_DIR/Contents/Home/bin:$PATH
export JAVA_HOME=$GRAALVM_DIR/Contents/Home
```

## Build & Run

You can run the application as a standard Micronaut application with `./gradlew run`. For creating the `native-image` you can use the `Makefile` in the project and run `make native-image`. Running the command creates a `kotlin-micronaut` binary, weighting around ~50MBs.