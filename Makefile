.PHONY: help

help: ## Displays the available commands (this help)
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.DEFAULT_GOAL := help

native-image: ## build a native executable
	@./gradlew shadowJar
	@java -cp build/libs/kotlin-micronaut-*.jar io.micronaut.graal.reflect.GraalClassLoadingAnalyzer
	@native-image --no-server \
       --class-path build/libs/kotlin-micronaut-*.jar \
       -H:ReflectionConfigurationFiles=build/reflect.json \
       -H:EnableURLProtocols=http \
       -H:IncludeResources="logback.xml|application.yml" \
       -H:Name=kotlin-micronaut \
       -H:Class=co.makery.Application \
       -H:+ReportUnsupportedElementsAtRuntime \
       -H:+AllowVMInspection \
       --allow-incomplete-classpath \
       --rerun-class-initialization-at-runtime='sun.security.jca.JCAUtil$CachedSecureRandomHolder,javax.net.ssl.SSLContext' \
       --delay-class-initialization-to-runtime=io.netty.handler.codec.http.HttpObjectEncoder,io.netty.handler.codec.http.websocketx.WebSocket00FrameEncoder,io.netty.handler.ssl.util.ThreadLocalInsecureRandom,com.sun.jndi.dns.DnsClient
